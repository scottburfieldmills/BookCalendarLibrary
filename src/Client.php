<?php

namespace GoodReadsClient;

class Client
{
    /**
     * API Key provided by Good Reads
     *
     * @var string
     */
    private $apiKey;

    /**
     * API Secret provided by Good Reads
     *
     * @var string
     */
    private $apiSecret;

    /**
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct($apiKey, $apiSecret)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }


}